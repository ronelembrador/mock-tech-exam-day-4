let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
  
	return collection;

}

// Adds element to the rear of the queue
function enqueue(e) {

	collection.push(e)
	return collection

}

// Removes element from the front of the queue
function dequeue() {
  
			collection.shift()
			return collection

}

// Show element at the front
function front() {

	if (collection.length >= 1){
		return collection[0]
	} else {
		return "There are no elements in the array"
	}
  
}

// Show total number of elements
function size() {

	return collection.length
   
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {

	if (collection.length > 0){
		return false
	} else {
		return true
	}

}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
